package wac.rr;

import java.util.HashMap;

import sium.nlu.context.Context;
import sium.nlu.context.Entity;
import wac.rr.grounding.DiscGrounder;
import sium.nlu.stat.DistRow;
import sium.nlu.stat.Distribution;

public class MultipleWordsAsClassifiersRR extends WordsAsClassifiersRR {
	
	private HashMap<String,DiscGrounder<String, String> > grounders;
	private HashMap<String,Context<String,String>> contexts;
	private DiscGrounder<String,String> finalGrounder;
	private Context<String,String> finalContext; 
	
	public MultipleWordsAsClassifiersRR() {
		super();
		grounders = new HashMap<String,DiscGrounder<String, String>>();
		contexts = new HashMap<String,Context<String,String>>();
		finalContext = new Context<String,String>();
		finalGrounder = new DiscGrounder<String,String>();
	}
	
	public void newEpisode() {
		newReferringExpression();
		grounders.clear();
		finalGrounder.clear();
	}
	
	@Override
	public void newReferringExpression() {
		for (String id : grounders.keySet()) {
			grounders.get(id).clear();
		}
		grounders.clear();
		contexts.clear();
		finalGrounder.clear();
		finalContext = new Context<String,String>();
	}
	
	public void setContext(String id, Context<String,String> context) {
		contexts.put(id, context);
		grounders.put(id, new DiscGrounder<String,String>());
		for (Entity<String> entity : context.getEntities()) {
			finalContext.setEntity(entity.getEntity(), context.getPropertiesForEntity(entity.getEntity()));
		}
		
	}
	
	
	public Distribution<String> groundIncrement(String word) {
//		step through each of the images, get a distribution over the objects for each
		for (String id : contexts.keySet()) {
			Distribution<String> inc = applyIncrement(contexts.get(id), word);
			grounders.get(id).groundIncrement(contexts.get(id), inc);
			inc = applyIncrement(finalContext, word);
			finalGrounder.groundIncrement(finalContext, inc);
		}
		
//		now, take the arg max from each of the above distributions and make a final distribution out of that
//		Distribution<String> argMaxs = new Distribution<String>();
//		for (String id : grounders.keySet()) {
//			Distribution<String> post = grounders.get(id).getPosterior();
//			if (post != null && !post.isEmpty()) {
//				DistRow<String> max = post.getArgMax();
//				argMaxs.addProbability(id, max.getProbability()); //<-- note that the entity id is the one for the image
//			}
//		}
////		keep track if there are multiple referring expressions (segments) by doing a piecewise multiplication
//		finalGrounder.groundIncrement(null, argMaxs);
		
		return finalGrounder.getPosterior();
	}
	
	public Distribution<String> getCurrentIncrementDistribution() {
		return finalGrounder.getPosterior();
	}
	
	public Distribution<String> crumble() {
		Distribution<String> post = finalGrounder.getPosterior();
		
		Distribution<String> mrr = new Distribution<String>();
		
		int rank = 1;
		for (DistRow<String> row : post.getDistribution()) {
			String imageID = row.getEntity().split("_")[1];
			Double soFar = mrr.getProbabilityForItem(imageID);
			mrr.setProbabilityForItem(imageID, (1.0 / rank * row.getProbability()) + soFar);
			rank++;
		}
		
		return mrr;
	}
	
	

}
