package wac.rr.prob;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.mahout.classifier.sgd.L1;
import org.apache.mahout.classifier.sgd.L2;
import org.apache.mahout.classifier.sgd.OnlineLogisticRegression;
import org.apache.mahout.common.RandomUtils;
import org.apache.mahout.math.Vector;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;

public class LogisticRegression {
	
	OnlineLogisticRegression lr;
	
	@SuppressWarnings("resource")
	public LogisticRegression (int numFeatures, double rate, double lambda) {
		// lambda here needs to be relatively small to avoid swamping the actual signal, but can be
	    // larger than usual when the data are dense.  The learning rate should also be quite small.

		lr = new OnlineLogisticRegression(2, numFeatures, new L1()) //the second number is the number of features for each vector that is expected
        .learningRate(rate)
        .alpha(1)
        .lambda(lambda)
        ;
		
	}
	
	public void trainSet(List<Integer> target, List<Vector> data) {
		 
		Set<Integer> range =  ContiguousSet.create(Range.closed(0, data.size()-1), DiscreteDomain.integers());
		List<Integer> indeces = new ArrayList<Integer>(range);
		Random random = RandomUtils.getRandom();
		
	     for (int pass = 0; pass < 50; pass++) { 
	    	Collections.shuffle(indeces, random);
		    for (Integer row : indeces) {
		    	trainIndividual(target.get(row), data.get(row));
		     }	
	     }
	}
	      
	private void trainIndividual(Integer targetRow, Vector trainRow) {
//		System.out.println(targetRow + " " + trainRow);
		lr.train((int)targetRow, trainRow);
	}
	
	public double getTrueProbability(Vector v) {
		return getProbabilities(v).get(0);
	}
	
	public Vector getProbabilities(Vector v) {
		return lr.classifyFull(v);
	}
	
	public void close() {
		lr.close();
	}
	
	
}
