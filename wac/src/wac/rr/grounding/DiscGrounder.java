package wac.rr.grounding;

import sium.nlu.context.Context;
import sium.nlu.grounding.Grounder;
import sium.nlu.stat.DistRow;
import sium.nlu.stat.Distribution;

public class DiscGrounder<E,P extends Comparable<P>> extends Grounder<E,P> {
	
	public DiscGrounder() {
		super();
	}
	
	public void groundIncrement(Context<E, P> context, Distribution<E> inc) {
		
		setPrior(getPosterior());
		
		Distribution<E> newDist = new Distribution<E>();
		for (DistRow<E> entity: inc.getDistribution()) {
			
			double sum = entity.getProbability();
	        // treat an empty prior as a uniform distribution
			if (getPrior().isEmpty()) {
				newDist.addProbability(entity.getEntity(), sum);
			}
			else {
				newDist.addProbability(entity.getEntity(), sum + getPrior().getProbabilityForItem(entity.getEntity().toString()));
			}
		}
		
		addToHistory(newDist);
		setPosterior(newDist);
		
	}

}
