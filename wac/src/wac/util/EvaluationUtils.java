package wac.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.mahout.math.Vector;

import sium.nlu.context.Context;
import sium.nlu.context.Entity;
import wac.rr.grounding.DiscGrounder;
import sium.nlu.grounding.Grounder;
import sium.nlu.language.LingEvidence;
import sium.nlu.stat.Distribution;
import wac.rr.prob.LogisticRegression;

public class EvaluationUtils {
	
	public static Distribution<String> evaluateEpisode(HashMap<String, LogisticRegression> classifiers, 
					Context<String,String> context, ArrayList<LingEvidence> ling) throws SQLException {
			
    	Grounder<String, String> grounder = new DiscGrounder<String, String>();
    	
    	for (LingEvidence l : ling) {
    		Distribution<String> inc = evaluateEpisodeIncrement(classifiers, context, l.getValue("w1"));
    		if (inc == null) continue;
    		grounder.groundIncrement(context, inc);
    	}
    	
//    	double numWords = ling.size();
//    	for (Entity<String> entity : context.getEntities()) {
//    		dist.addProbability(entity.getEntity(), dist.getProbabilityForItem(entity.getEntity()) / numWords);
//    	}
//    	dist.normalize();
    	
    	return grounder.getPosterior();
	
	}

	public static Distribution<String> evaluateEpisodeIncrement(HashMap<String, LogisticRegression> classifiers, 
			                                                    Context<String,String> context, String w) {
		
		synchronized (context) {
			
			Distribution<String> inc = new Distribution<String>();
			
			
			if (!classifiers.containsKey(w)) return null;
			
			LogisticRegression wordClassifier = classifiers.get(w);
			
			for (Entity<String> entity : context.getEntities()) {
				
				Vector v = VectorUtils.makeVector(context.getPropertiesForEntity(entity.getEntity()));
				Double prob = wordClassifier.getTrueProbability(v);
				
				inc.addProbability(entity.getEntity(), prob);
			}	
			
			return inc;
		
		}
	
	}

	public static double evaluateThreshold(LogisticRegression logisticRegression, Vector sample) {
		return logisticRegression.getTrueProbability(sample);
	}


}
