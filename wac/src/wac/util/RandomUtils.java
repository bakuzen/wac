package wac.util;

import java.util.Random;
import java.util.Set;

import sium.nlu.context.Context;
import sium.nlu.context.Entity;

public class RandomUtils {
	
	public static String chooseRandomNotGold(Context<String, String> context, String gold) {
		
		Random rand = new Random();
		Set<Entity<String>> entities = context.getEntities();
		int i = rand.nextInt(entities.size());
		
		String nonGold = new String();
		int j = 0;
		for (Entity<String> e : entities) {
			if (e.getEntity().equals(gold)) continue;
			nonGold = e.getEntity();
			if (j >= i) break;
			j++;
		}
		if (gold.equals(nonGold)) throw new RuntimeException();
		return nonGold;
	}

}
