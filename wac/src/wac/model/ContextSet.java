package wac.model;

import java.util.ArrayList;
import java.util.HashMap;

import sium.nlu.context.Context;

public class ContextSet {
	
	private HashMap<String, HashMap<String, Context<String,String>>> contexts;
	
	public ContextSet() {
		contexts = new HashMap<String, HashMap<String, Context<String,String>>>();
	}
	
	public void makeSet(String set) {
		if (!contexts.containsKey(set)) {
			contexts.put(set, new HashMap<String, Context<String,String>>());
		}
	}
	
	public void addContext(String set, String imageID, Context<String,String> context) {
		makeSet(set);
		contexts.get(set).put(imageID, context);
	}
	
	public boolean hasSet(String set) {
		return contexts.containsKey(set);
	}

	
	public HashMap<String,Context<String,String>> getContextsForSet(String set) {
		return contexts.get(set);
	}

	public void addEntity(String set, String imageID, String entity, ArrayList<String> info) {
		
		makeSet(set);
		
		if (!contexts.get(set).containsKey(imageID)) {
			contexts.get(set).put(imageID, new Context<String,String>());
		}
		
		Context<String,String> context = contexts.get(set).get(imageID);
		
		context.addPropertyToEntity(entity, "r:"+info.get(4));
		context.addPropertyToEntity(entity, "g:"+info.get(5));
		context.addPropertyToEntity(entity, "b:"+info.get(6));
		context.addPropertyToEntity(entity, "h:"+info.get(7));
		context.addPropertyToEntity(entity, "s:"+info.get(8));
		context.addPropertyToEntity(entity, "v:"+info.get(9));
		context.addPropertyToEntity(entity, "ne:"+info.get(13));
		context.addPropertyToEntity(entity, "x:"+info.get(15));
		context.addPropertyToEntity(entity, "y:"+info.get(16));
		
	}

	public Context<String, String> getContext(String episodeID, String imageID) {
		return contexts.get(episodeID).get(imageID);
	}

	public boolean hasImage(String episodeID, String imageID) {
		return contexts.get(episodeID).containsKey(imageID);
	}
	

}
