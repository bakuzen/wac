package wac.prob;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.apache.mahout.math.Vector;
import org.junit.Test;

import sium.nlu.context.Context;
import sium.nlu.context.Entity;
import sium.nlu.context.Properties;
import sium.nlu.context.Property;
import sium.nlu.language.LingEvidence;
import sium.nlu.stat.Distribution;
import wac.model.ContextSet;
import wac.model.Episode;
import wac.rr.MultipleWordsAsClassifiersRR;
import wac.rr.WordsAsClassifiersRR;
import wac.util.OnlineGameUtils;
import wac.util.RandomUtils;
import wac.util.VectorUtils;

public class OnlineGameTest {

	@Test
	public void run() {
		try {
			
			
			int numNegs = 4;			
			
			
			ContextSet contexts = OnlineGameUtils.readFeaturesFile("domains/game/features.txt");
			ArrayList<Episode> episodes = OnlineGameUtils.readEpisodesFile("domains/game/text.txt");
			
			System.out.println("number of episodes: "  + episodes.size());
			
			for (int iter=0; iter<5; iter++) {

				String evalSet = "Set" + iter;
				System.out.println("===========\n" +evalSet);
				
				ArrayList<Episode> forEval = new ArrayList<Episode>();
				MultipleWordsAsClassifiersRR wacrr = new MultipleWordsAsClassifiersRR();
				
				for (Episode episode : episodes) {
					if (episode.getEpisodeID().contains(evalSet)) {
						forEval.add(episode);
						continue;
					}
					
					HashMap<String, Context<String, String>> contextSet = contexts.getContextsForSet(episode.getEpisodeID());
					
					if (!contexts.hasImage(episode.getEpisodeID(), episode.getImageID())) continue;
					if (!contexts.getContext(episode.getEpisodeID(), episode.getImageID()).containsEntity(episode.getReferent())) continue;
					
					Vector positive = VectorUtils.makeVector(contexts.getContext(episode.getEpisodeID(), episode.getImageID()).getPropertiesForEntity(episode.getReferent()));
					ArrayList<String> contextKeys = new ArrayList<String>(contextSet.keySet());
					contextKeys.remove(episode.getEpisodeID());
					
					for (LingEvidence ling : episode.getWords()) {
						String word = ling.getValue("w1");
						wacrr.addPositiveTrainingSample(word, positive);
	//					for negative examples...
						for (int n=0; n<numNegs; n++) {
							if (contextKeys.isEmpty()) break;
							int item = new Random().nextInt(contextKeys.size());
	//						grab a random context (i.e., all the object info from an image not the referred one)
							Context<String,String> randNegContext = contexts.getContext(episode.getEpisodeID(), contextKeys.get(item));
							for (Entity<String> negEntity : randNegContext.getEntities()) {
								Vector negative = VectorUtils.makeVector(randNegContext.getPropertiesForEntity(negEntity.getEntity()));
								wacrr.addNegativeTrainingSample(word, negative);
								n++;
							}
							contextKeys.remove(contextKeys.get(item));
						}
					}
				} 	
				
				wacrr.train();
				System.out.println("vocab size: " + wacrr.getVocabularySize());
				double correct = 0.0;
				double total = 0.0;
				for (Episode episode : forEval) {
					
					HashMap<String, Context<String, String>> context = contexts.getContextsForSet(episode.getEpisodeID());
					
					wacrr.newReferringExpression();
					
					for (String id : context.keySet()) {
						wacrr.setContext(id, context.get(id));
					}
	
					for (LingEvidence ling : episode.getWords()) {
						String word = ling.getValue("w1");
						if (word.equals("and") || word.equals("got") || word.equals("it")) {
							continue;
						}					
			    		 wacrr.groundIncrement(word);
					}
					
					Distribution<String> fdist  = wacrr.crumble();
					
					if (fdist.isEmpty()) {
	//					System.out.println("problem with episode " + episode.getReferent());
						continue;
					};
					
					String imageIDGuess = fdist.getArgMax().getEntity(); // we are only interested in the correct image (which could have several objects)
					if (imageIDGuess.equals(episode.getImageID())) 
						correct++;
					total++;
				}
				
				System.out.println(+correct + " " + total + " " + (correct/total));
			}
			
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	
}
