package wac.prob;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.RandomAccessSparseVector;
import org.apache.mahout.math.Vector;

import sium.nlu.context.Context;
import sium.nlu.context.Entity;
import sium.nlu.context.Properties;
import sium.nlu.context.Property;
import sium.nlu.language.LingEvidence;
import sium.nlu.stat.DistRow;
import sium.nlu.stat.Distribution;
import sium.system.util.TakeCVSqlUtils;
import sium.system.util.TakeSqlUtils;

import com.google.common.collect.Lists;

import org.junit.Test;

import sium.system.util.FileUtils;
import wac.rr.ThresholdModel;
import wac.rr.WordsAsClassifiersRR;
import wac.rr.prob.LogisticRegression;
import wac.util.EvaluationUtils;
import wac.util.RandomUtils;
import wac.util.TakeRawSqlUtils;
import wac.util.VectorUtils;
import static org.junit.Assert.*;

public class LogisticRegressionTest {
	

	
 
	TakeCVSqlUtils take = new TakeCVSqlUtils();
	
	int numNegs = 1;
	
	
	@Test
	public void testLogRes() throws IOException {
		
	double max = 1000;
	

//	FileWriter outfile = new FileWriter(new File("/home/casey/Desktop/wac.txt"));
    try {
    	
		take.createConnection();
//		ArrayList<String> episodes = take.getAllEpisodes();
		ArrayList<String> episodes = take.getTargetOnlyEpisodes();
		ArrayList<String> landmarkEpisodes = take.getLandmarkEpisodes();

		Collections.shuffle(episodes);
		
		
		 List<String> train = episodes.subList(0, episodes.size()-100);
		 List<String> test = episodes.subList(episodes.size()-100, episodes.size());
		 System.out.println(test);
//		 System.out.println(test);
//		for (double lambda=1; lambda>=1e-10; lambda *= 0.1) {
//			for (double rate=10; rate>=1e-10; rate *= 0.1) {
				
			    
				
//				for (int iters=0; iters<max; iters++) {
					double mrr = 0.0;
					double correct = 0.0;
//					List<String> ctrain = episodes.subList(0, train);
					
					WordsAsClassifiersRR wacrr = new WordsAsClassifiersRR();
					
		//		    collect the training data
				    collectData(train, wacrr, "t", "referent");
				    collectData(landmarkEpisodes, wacrr, "l", "landmark");
				    
				    wacrr.train();
				    
				    double tp = 0.0;
				    double fp = 0.0;
				    double tn = 0.0;
				    double fn = 0.0;
				 
				    double thresh = 0.3;
				    
					for (String episode : train) {
				    	wacrr.newReferringExpression();
				    	Context<String,String> context = take.getRawFeatures(episode);
				    	
				    	ArrayList<LingEvidence> lingEv = take.getLingEvidence(episode, "t");
				    	
				
				    	for (LingEvidence ling : lingEv) {
				    		String word = ling.getValue("w1");
				    		wacrr.groundIncrement(context, word);
				    		
					    	Distribution<String> fdist = new Distribution<String>(wacrr.getCurrentIncrementDistribution());
					    	String gold = take.getGoldPiece(episode);
					    	String guess = wacrr.getCurrentIncrementArgMax();
					    	double margin = fdist.getMargins().get(0);
					    	if (gold.equals(guess)) {
					    		if (margin > thresh) tp++;
					    		else fn++;
					    	}
					    	else{
					    		if (margin < thresh) tn++;
					    		else fp++;
					    	}
				    	}
					}
				    

//				    ThresholdModel thresh = new ThresholdModel();
//				    trainThreshold(wacrr, thresh, train);
				    
				    
				    
		//		    evaluate
	
				    for (String episode : test) {
				    	wacrr.newReferringExpression();
				    	Context<String,String> context = take.getRawFeatures(episode);
				    	String gold = take.getGoldPiece(episode);
				    	ArrayList<LingEvidence> lingEv = take.getLingEvidence(episode, "td");
				    	for (LingEvidence ling : lingEv) {
				    		wacrr.groundIncrement(context, ling.getValue("w1"));
				    		Distribution<String> dist = new Distribution<String>(wacrr.getCurrentIncrementDistribution());
				    		if (dist.isEmpty()) continue;
//				    		double prob = thresh.applyIncrement(VectorUtils.makeVector(dist));
				    		
//				    		System.out.println(ling.getValue("w1") + " " + prob);
					    	Distribution<String> fdist = wacrr.getCurrentIncrementDistribution();
					    	
					    	String guess = wacrr.getCurrentIncrementArgMax();
					    	if (gold.equals(guess)) correct += 1.0;
				    		if (fdist.isEmpty()) continue;
					    	double margin = fdist.getMargins().get(0);
					    	if (gold.equals(guess)) {
					    		if (margin > thresh) tp++;
					    		else fn++;
					    	}
					    	else{
					    		if (margin < thresh) tn++;
					    		else fp++;
					    	}
				    	}
				    	
				    	Distribution<String> fdist = wacrr.getCurrentIncrementDistribution();
				    	int rank = fdist.findRank(gold);
				    	

				    	
				    	
				    	 
//				    	System.out.println(episode + " " + gold + " " + guess + " "+ rank);
				    	
				    	if (rank != -1) 
				    		mrr += 1.0 / (double) rank;
				    }
				    
				    double acc = correct / (100.0);
				    double rank = mrr / (100.0);
//			    	outfile.write(iters + " " + acc + " " + rank + "\n");
			    	wacrr.clear();
			    	
					double precision = tp / (tp + fp);
					double recall = tp / (tp + fn);
					System.out.println("fscore: " + (2 * (precision * recall) / (precision + recall)));
				  
//				}
				
//				outfile.close();
			    
//			}
					
//		}
	 
    }
	catch (SQLException e) {
		e.printStackTrace();
	}
			
			
	}

	

	private void trainThreshold(WordsAsClassifiersRR wacrr, ThresholdModel thresh, List<String> train) throws SQLException {
		
    	int pos =0;
    	int neg = 0;
    	ArrayList<Vector> negatives = new ArrayList<Vector>();
    	
		for (String episode : train) {
	    	wacrr.newReferringExpression();
	    	Context<String,String> context = take.getRawFeatures(episode);
	    	
	    	ArrayList<LingEvidence> lingEv = take.getLingEvidence(episode, "t");
	    	
	
	    	for (LingEvidence ling : lingEv) {
	    		String word = ling.getValue("w1");
	    		wacrr.groundIncrement(context, word);
	    		
		    	Distribution<String> fdist = new Distribution<String>(wacrr.getCurrentIncrementDistribution());
		    	String gold = take.getGoldPiece(episode);
		    	String guess = wacrr.getCurrentIncrementArgMax();
//				positive example
		    	if (gold.equals(guess)) {
		    		thresh.addPositiveTrainingSample(VectorUtils.makeVector(fdist));
		    		pos ++;
		    	}
//		    	negative example
		    	else {
		    		negatives.add(VectorUtils.makeVector(fdist));
		    		neg ++;
		    	}
	    	}
//	    	inc++;
	    	
		}
		for (Vector v: negatives.subList(0, pos)) thresh.addNegativeTrainingSample(v); 
		
		thresh.train();
		
		
	}



	private void collectData(List<String> episodes, WordsAsClassifiersRR wacrr, String tags, String table) throws SQLException {
		
		
		 for (String episode: episodes) {
		    	Context<String,String> context = take.getRawFeatures(episode);
		    	ArrayList<LingEvidence> ling = take.getLingEvidence(episode, tags);
		    	String gold = take.getGoldPiece(episode, table);
		    	
		    	Vector pos = VectorUtils.makeVector(context.getPropertiesForEntity(gold));

		    	for (LingEvidence l : ling) {
		    		String word = l.getValue("w1");

		    		wacrr.addPositiveTrainingSample(word, pos);
		    		
		    		for (int i=0; i<numNegs; i++) {
		    			String nonGold = RandomUtils.chooseRandomNotGold(context, gold);
		    			Vector neg = VectorUtils.makeVector(context.getPropertiesForEntity(nonGold));
		    			wacrr.addNegativeTrainingSample(word, neg);
		    		}
		    	}
		    }
	}
	




	

}
