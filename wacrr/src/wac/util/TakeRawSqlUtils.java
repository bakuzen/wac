package wac.util;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import sium.nlu.context.Context;
import sium.system.util.TakeSqlUtils;

public class TakeRawSqlUtils extends TakeSqlUtils {
	
	public Context<String,String> getRawFeatures(String episode) throws SQLException {
		
		Context<String,String> context = new Context<String,String>();
		
		ResultSet pieces = this.getRawFeaturesForEpisode(episode);
		
		while (pieces.next()) {
			String entity = pieces.getString("id");
			context.addPropertyToEntity(entity, "r:"+pieces.getString("r"));
			context.addPropertyToEntity(entity, "g:"+pieces.getString("g"));
			context.addPropertyToEntity(entity, "b:"+pieces.getString("b"));
			context.addPropertyToEntity(entity, "h:"+pieces.getString("h"));
			context.addPropertyToEntity(entity, "s:"+pieces.getString("s"));
			context.addPropertyToEntity(entity, "v:"+pieces.getString("v"));
//			context.addPropertyToEntity(entity, "o:"+pieces.getString("orientation"));
//			context.addPropertyToEntity(entity, "hs:"+pieces.getString("h_skew"));
//			context.addPropertyToEntity(entity, "vs:"+pieces.getString("v_skew"));
			context.addPropertyToEntity(entity, "ne:"+pieces.getString("num_edges"));
			context.addPropertyToEntity(entity, "x:"+pieces.getString("pos_x"));
			context.addPropertyToEntity(entity, "y:"+pieces.getString("pos_y"));
			
		}
		
		pieces.close();
		
		
		return context;
	}

	
	public ResultSet getRawFeaturesForEpisode(String episode) throws SQLException {
		Statement stat = createStatement();
		return stat.executeQuery(String.format("select * from cv_piece_raw where episode_id='%s'", episode));
	}

}
