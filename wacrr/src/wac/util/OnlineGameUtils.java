package wac.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import sium.nlu.context.Context;
import sium.nlu.language.LingEvidence;
import wac.model.ContextSet;
import wac.model.Episode;

public class OnlineGameUtils {
	
	
	public static ContextSet readFeaturesFile(String featuresFile) throws FileNotFoundException {
		
		Scanner scan = new Scanner(new File(featuresFile));
		
		ContextSet contexts = new ContextSet();  
		
		
		scan.nextLine(); // first line is column headers; ignore
		while (scan.hasNext()) {
			String line = scan.nextLine();

			ArrayList<String> info = new ArrayList<String>(Arrays.asList(line.split("\\t")));
			
			String entity = makeEntityID(info.get(1), info.get(2));
			contexts.addEntity(info.get(3), info.get(2), entity, info);
			
		}
		
		scan.close();
		
		return contexts;
		
		
	}

	public static String makeEntityID(String id, String img) {
		
		return String.format("%s_%s", id, img);
		
	}

	public static ArrayList<Episode> readEpisodesFile(String filePath) throws FileNotFoundException {
		
		ArrayList<Episode> episodes = new ArrayList<Episode>();
		
		Scanner scan = new Scanner(new File(filePath));
		
		scan.nextLine(); // first line is the header

		
		
		while (scan.hasNext()) {
			String line = scan.nextLine();
			ArrayList<String> info = new ArrayList<String>(Arrays.asList(line.split("\\t")));
			
			Episode episode = new Episode(info.get(0), info.get(1), info.get(2));
			
			ArrayList<LingEvidence> lings = new  ArrayList<LingEvidence>();
			String[] words = info.get(3).trim().split("\\s+");
			for (int i=0; i<words.length; i++) {
				LingEvidence ling = new LingEvidence();
				ling.addEvidence("w1", words[i].toLowerCase());
				lings.add(ling);
			}
			episode.setWords(lings);
			episodes.add(episode);
		}
		
		
		scan.close();
		return episodes;
	}

	public static String getLeft(String episode) {
		return episode.split("_")[0];
	}

	public static String getRight(String episode) {
		return episode.split("_")[1];
	}
}
