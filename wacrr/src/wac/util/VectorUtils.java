package wac.util;

import org.apache.mahout.math.RandomAccessSparseVector;
import org.apache.mahout.math.Vector;

import sium.nlu.context.Properties;
import sium.nlu.context.Property;
import sium.nlu.stat.DistRow;
import sium.nlu.stat.Distribution;

public class VectorUtils {
	
	public static Vector makeVector(Properties<Property<String>> entityProps) {
		
		Vector v = new RandomAccessSparseVector(entityProps.size());
		
		int i = 0;
		for (Property<String> prop : entityProps) {
			String[] p = prop.getProperty().split(":");
			v.set(i, Double.parseDouble(p[1]));
			i++;
		}
		return v;
	}
	
	public static Vector makeVector(Distribution<String> dist) {

		dist.normalize();
		
		int max = 1;
		
		if (dist.size() < max) {
			max = dist.size();
		}
		int size = max * 2; // take top 10 values, 9 differences, and 1 entropy (of entire dist), and 1 inc of word
		
		Vector v = new RandomAccessSparseVector(size);
		
		int i = 0;
//		for (DistRow<String> prop : dist.getTopN(max)) {	
//			v.set(i, prop.getProbability()); 
//			i++;
//		}
//		for (Double margin : dist.getMargins().subList(0, max-1)) {
//			v.set(i, margin);
//			i++;
//		}
		v.set(i++, dist.getMargins().get(0));

		double entropy = dist.getEntropy();
		if (Double.isNaN(entropy)) entropy = 1000.0;
		v.set(i++, entropy);
		return v;
	}

}
