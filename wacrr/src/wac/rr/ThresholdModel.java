package wac.rr;


import org.apache.mahout.math.Vector;

import wac.rr.prob.LogisticRegression;
import wac.util.EvaluationUtils;


public class ThresholdModel extends WordsAsClassifiersRR {
	
	private final String THRESH_WORD = "<thresh>";
	
	
	public ThresholdModel() {
		super();
	}
	
	public void train() {
		for (String word : data.keySet()) {
	    	LogisticRegression lr = new LogisticRegression(data.get(word).get(0).size(), 0.001, 1.0); 
	    	lr.trainSet(target.get(word), data.get(word));
	    	lr.close();
	    	classifiers.put(word,  lr);
	    }		
	}
	
	
	public void addPositiveTrainingSample(Vector sample) {
		addTrainingSample(THRESH_WORD, sample, POSITIVE);
	}
	
	public void addNegativeTrainingSample(Vector sample) {
		addTrainingSample(THRESH_WORD, sample, NEGATIVE);
	}
	
	public double applyIncrement(Vector sample) {
		return EvaluationUtils.evaluateThreshold(classifiers.get(THRESH_WORD), sample);
	}
	
	
}
