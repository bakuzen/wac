package wac.rr;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.mahout.math.Vector;

import sium.nlu.context.Context;
import wac.rr.grounding.DiscGrounder;
import sium.nlu.grounding.Grounder;
import sium.nlu.language.LingEvidence;
import sium.nlu.stat.Distribution;
import wac.rr.prob.LogisticRegression;
import wac.util.EvaluationUtils;

public class WordsAsClassifiersRR {
	
	
	protected final int POSITIVE = 0;
	protected final int NEGATIVE = 1;
	private final String WORD_VALUE = "w1";

    protected HashMap<String,List<Vector>> data;
    protected HashMap<String,List<Integer>> target;
    protected HashMap<String,LogisticRegression> classifiers;
    private Grounder<String, String> grounder;
    
    public HashMap<String,LogisticRegression> getClassifiers() {
    	return classifiers;
    }
	
	public WordsAsClassifiersRR() {
		data = new HashMap<String,List<Vector>>();
		target = new HashMap<String,List<Integer>>();
		classifiers = new HashMap<String,LogisticRegression>();
		grounder = new DiscGrounder<String, String>();
	}
	
	public WordsAsClassifiersRR(HashMap<String,LogisticRegression> classifiers) {
		this();
		this.classifiers = classifiers;
	}
	
	public int getVocabularySize() {
		return data.size();
	}
	
	public int getTrainingDataSize() {
		int tot = 0;
		for (String w : data.keySet()) 
			tot += data.get(w).size();
		return tot;
	}

	public void addTrainingSample(String word, Vector sample, int i) {
		if (!data.containsKey(word)) {
			data.put(word, new ArrayList<Vector>());
			target.put(word, new ArrayList<Integer>());
		}		
		data.get(word).add(sample);
		target.get(word).add(i);		
	}

	public void addPositiveTrainingSample(String word, Vector sample) {
		addTrainingSample(word, sample, POSITIVE);
	}
	
	public void addNegativeTrainingSample(String word, Vector sample) {
		addTrainingSample(word, sample, NEGATIVE);
	}

	public void train() {
		for (String word : data.keySet()) {
	    	LogisticRegression lr = new LogisticRegression(data.get(word).get(0).size(), 0.001, 1.0); 
	    	lr.trainSet(target.get(word), data.get(word));
	    	lr.close();
	    	classifiers.put(word,  lr);
	    }		
	}
	
	public Distribution<String> getCurrentIncrementDistribution() {
		return grounder.getPosterior();
	}
	
	public String getCurrentIncrementArgMax() {
		if (grounder != null && grounder.getPosterior() != null &&  
			!grounder.getPosterior().isEmpty() && grounder.getPosterior().getArgMax() != null) {
			return grounder.getPosterior().getArgMax().getEntity();
		}
		return null;
	}
	
	public double getCurrentIncrementArgMaxProbability() {
		if (grounder != null && grounder.getPosterior() != null &&  
			!grounder.getPosterior().isEmpty() && grounder.getPosterior().getArgMax() != null) {
			return grounder.getPosterior().getArgMax().getProbability();
		}
		return 0.0; // <-- watch out!!
	}
	
	public Distribution<String> groundIncrement(Context<String, String> context, String word) {
		Distribution<String> inc = applyIncrement(context, word);
		grounder.groundIncrement(context, inc);
		return grounder.getPosterior();
	}
	
	public Distribution<String> applyIncrement(Context<String, String> context, LingEvidence ling) {
		return EvaluationUtils.evaluateEpisodeIncrement(classifiers, context, ling.getValue(WORD_VALUE));
	}
	
	public Distribution<String> applyIncrement(Context<String, String> context, String word) {
		return EvaluationUtils.evaluateEpisodeIncrement(classifiers, context, word);
	}

	public Distribution<String> applyReferringExpression(Context<String, String> context, ArrayList<LingEvidence> ling) throws SQLException {
		return EvaluationUtils.evaluateEpisode(classifiers, context, ling);
	}
	
	public void newReferringExpression() {
		grounder.clear();
	}
	
	public void clear(){
		data.clear();
	    target.clear();
	    classifiers.clear();
	}
}
