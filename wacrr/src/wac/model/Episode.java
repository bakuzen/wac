package wac.model;

import java.util.ArrayList;

import sium.nlu.language.LingEvidence;
import wac.util.OnlineGameUtils;

public class Episode {
	
	private ArrayList<LingEvidence> words;
	private String referent;
	private String imageID;
	private String episodeID; 
	
	public Episode() {
		words = new ArrayList<LingEvidence>();
	}
	
	public Episode(String referent, String imageID, String episodeID) {
		this();
		this.referent = referent;
		this.imageID = imageID;
		this.episodeID = episodeID;
	}
	
	public ArrayList<LingEvidence> getWords() {
		return words;
	}

	public void setWords(ArrayList<LingEvidence> words) {
		this.words = words;
	}

	public String getReferent() {
		return OnlineGameUtils.makeEntityID(this.referent, this.getImageID());
	}

	public void setReferent(String referent) {
		this.referent = referent;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}

	public String getEpisodeID() {
		return episodeID;
	}

	public void setEpisodeID(String episodeID) {
		this.episodeID = episodeID;
	}
	

}
